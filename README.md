```
__/\\\________/\\\__/\\\\\\\\\\\\_____/\\\\____________/\\\\_____/\\\\\\\\\____        
 _\/\\\_______\/\\\_\/\\\////////\\\__\/\\\\\\________/\\\\\\___/\\\\\\\\\\\\\__       
  _\/\\\_______\/\\\_\/\\\______\//\\\_\/\\\//\\\____/\\\//\\\__/\\\/////////\\\_      
   _\/\\\_______\/\\\_\/\\\_______\/\\\_\/\\\\///\\\/\\\/_\/\\\_\/\\\_______\/\\\_     
    _\/\\\_______\/\\\_\/\\\_______\/\\\_\/\\\__\///\\\/___\/\\\_\/\\\\\\\\\\\\\\\_    
     _\/\\\_______\/\\\_\/\\\_______\/\\\_\/\\\____\///_____\/\\\_\/\\\/////////\\\_   
      _\//\\\______/\\\__\/\\\_______/\\\__\/\\\_____________\/\\\_\/\\\_______\/\\\_  
       __\///\\\\\\\\\/___\/\\\\\\\\\\\\/___\/\\\_____________\/\\\_\/\\\_______\/\\\_ 
        ____\/////////_____\////////////_____\///______________\///__\///________\///__
```

# Server

The server is provided as a C header. For the moment this file is Xilinx specific more vendors will be considered in future updates. This file provides one external function called:

```C
void process_command(u32 *recv_buf, int sd)
```

This function is interface agnostic. So in principle if the ```sd``` you provide supports the ```write``` function it should work. Currently it was only tested with Ethernet but Serial support will be considered for future updates.

# Client

The client is a python CLI based on Cmd2 library. To install the Python app you just need to clone the repository and open a terminal inside the repository root folder. Then type:

```Bash
pip3 install .
```

or

```Bash
pip install .
```

Depending on the environment variable name you gave to your python3-pip path. It is recommended to run this in a python-env to avoid crossed requirements between different packages or apps that might break the install.  

After the install you can execute the application by running ```udma``` in any terminal.

## Command List

Here is a list of the available commands specific to the application. The cmd2 package includes more commands that help the user such as the ```help``` command which can be used to get more information regarding the application and its commands.

### Board communication

|||
|-|-|
|connect|Create the connect command to allow communication with the board via Ethernet|
|log|Starts serial logging to debug the transmission and processing of the messages|
|udma|Create the x_udma command to pass the UDMA instruction to the specified LRA |

>Note: UDMA function is not completely implemented and must not be used unless specified in the release notes

### Comblock Read

|||
|-|-|
|x_read_fifo|Create the x_read_fifo command to allow reading the FIFO of the Comblock|
|x_read_mem|Create the x_read_ram command to allow reading the RAM of the Comblock|
|x_read_ram|Create the x_read_ram command to allow reading the RAM of the Comblock|
|x_read_reg|Create the x_read_reg command to allow reading registers from the Comblock|

### Comblock Write

|||
|-|-|
|x_write_fifo|Create the x_write_fifo command to allow writing the FIFO of the Comblock|
|x_write_mem|Create the x_write_ram command to allow writing the RAM of the Comblock|
|x_write_ram|Create the x_write_ram command to allow writing the RAM of the Comblock|
|x_write_reg|Create the x_write_reg command to allow writing the registers of the Comblock|

# Usage

1. Open SDK
2. Create a FreeRTOS project based on the LWIP Echo.
3. Import 'udma.h' as a File System into the 'src' folder of the project.
4. Build the project, at this point no errors should show up.
5. Make a call to the process_message function inside 'echo.c' (or use the echo.c provided in the examples folder of this repo.).
6. Build again.
7. Load the bitstream onto the FPGA and program the ARM.
8. Go to the PC, open python and connect via the CLI. The default socket in the SoC is 192.168.1.10:7 if no DHCP is present.
9. Perform the read and write operations from the PC.
