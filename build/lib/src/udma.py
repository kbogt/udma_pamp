#!/usr/bin/python
#
# Copyright (C) 2018 INTI
# Copyright (C) 2018 Bruno Valinoti
# Copyright (C) 2020 Werner Florian

import socket, pathlib, re, sys, os, glob, cmd2, argparse, tqdm, appdirs
import src.intro as intro
import numpy as np
from time import sleep 

from cmd2.ansi import style
from struct import *

valid = 'abcdefxABDCEFX0123456789,'

# COMAND CODES
READ_REG          = 0
READ_RAM          = 1
READ_MEM          = 2
READ_FIFO         = 3
###########################
WRITE_REG         = 4
WRITE_RAM         = 5
WRITE_MEM         = 6
WRITE_FIFO        =	7
###########################
UDMA              = 8
###########################
ADC500_CFG        = 100
SET_DEC_N         = 101
PAMP_INIT         = 102
VADJ_SET          = 103
PAS_SET           = 104
###########################
CHANGE_CB         = 127
LOG               = 255

HV_SET            = 150
HV_PWRDOWN        = 151
VMON_READ			    = 152
IMON_READ			    = 153

#########################3
ECAL_SET_CH       = 300
ECAL_SET_ALL      = 301


BUFFER_SIZE = 4096

def tsocket(s):
  """Defines a new type called socket for the argparse to process.

  Parameters
  ----------
  s : str
    The data to parse into the new type socket

  Returns
  -------
  string
    The ip address that is sliced from the string
  int
    The port of the socket
  """
  try:
    ip, port = s.split(':')
    return ip, int(port)
  except:
    raise argparse.ArgumentTypeError("Enter a valid socket.")

def pdata(s): 
  """Defines a new type called pdata for the argparse to process

  Parameters
  ----------
  s : str
    The data to parse into the new type socket

  Returns
  -------
  list
    The data formatted as a list of numbers or a single int depending on the length
  """
  try:
    if all([c in valid for c in s]): # it is not a file
      return (1, s.split(','))
    else:
      file = pathlib.Path(s)
      if file.exists():
        return (0, file)
      else:
        del file
        raise argparse.ArgumentError("Invalid file.")
  except:
    raise argparse.ArgumentError("Invalid data format. Only comma separeted data accepted 1,2,3 or single data 1.")

def getsize(filename):
  """Get the size of a file

  Parameters
  ----------
  filename : file
    The file from which we want to get the size in lines

  Returns
  -------
  int
    The number of lines in the file
  """
  count = 0
  for line in open(filename).readlines(): count += 1
  return count

class UDMA_cli(cmd2.Cmd):
  """Defines the cli class with the extra attributes and functions related to the UDMA app

  Parameters
    ----------
    cmd2.Cmd: Cmd
      Class that defines the basic CLI functionality 

    Returns
    -------
    object
      UDMA_cli instance
  """
  # Command categories
  CMD_CAT_COMMUNICATION = 'Board communication'
  CMD_CAT_CMB_READ = 'Comblock Read'
  CMD_CAT_CMB_WRITE = 'Comblock Write'
  CMD_CAT_PAMP = "pAmp Control"

  app_name = 'udma'
  app_author = 'udma'

  def __init__(self):
    self.appdirs = appdirs.AppDirs(self.app_name, self.app_author)
    shortcuts = {'?': 'help', '!': 'shell', '$?': 'exit_code'}
    super().__init__(
            #use_ipython= True,
            persistent_history_file= self.history_file,
            persistent_history_length= 1000,
            shortcuts= shortcuts,
            allow_cli_args= False
            )
    self.self_in_py = True
    self.histfile = ".udma_history"
    self.allow_cli_args = False
    self.prompt = style('RVI CLI >: ', fg=cmd2.fg.blue, bold=True)
    self.intro =  style( intro.intro_small + intro.s_credits + 
      'This CLI application is the first edition of the UDMA on Cmd2. Use -h or --help for more information.', 
      fg=cmd2.fg.green, bg=cmd2.bg.black, bold=True) 
    self.allow_style = cmd2.ansi.STYLE_TERMINAL
    self.s = 0

  @property
  def history_file(self) -> str:
    if self.appdirs:
      return os.path.join(self.appdirs.user_config_dir, 'history.txt')
    return None

  def do_quit(self, args):
    """The quit command which closes the app
    """
    self.poutput ("Exiting.")
    try:
      self.s.close()
    except:
      pass
    return True

  do_exit = do_quit

  socket_paser = argparse.ArgumentParser()
  socket_paser.add_argument('-s', '--socket', type = tsocket, default='192.168.1.10:7', 
  help = 'Specify the filepath of the file to open.', metavar= 'xxx.xxx.xxx.xxx:port')
  @cmd2.with_category(CMD_CAT_COMMUNICATION)
  @cmd2.with_argparser(socket_paser)
  def do_connect(self, args):
    """The connect command to allow communication with the board via Ethernet
    """
    self.poutput("Address: %s" % (args.socket[0]))
    self.poutput("Port:    %s" % (args.socket[1]))
    try:
      self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
      self.s.connect((args.socket[0], args.socket[1]))
      return
    except:
      self.last_result = None
      self.perror("Connection refused")
      return

  @cmd2.with_category(CMD_CAT_COMMUNICATION)
  def do_disconnect(self, args):
    """The disconnect command to close the Ethernet socket.
    """
    try:
      self.s.close()
      return
    except:
      self.last_result = None
      self.perror("First stablish a connection using the connect command")
      return

  @cmd2.with_category(CMD_CAT_COMMUNICATION)
  def do_close_server(self, args):
    """Will close the tcp server on the board and closes the socket.
    """
    if not self.s:
      self.perror("Missing board connection, open the socket with the <connect> command.")
      return
    tx_buf  = pack("4s", bytes("quit",'utf-8'))
    self.s.send(tx_buf)
    self.s.close()
    return 

  def complete_x_write_mem(self, text, line, begidx, endidx):
    return self.path_complete(text, line, begidx, endidx, path_filter=os.path.isdir)

  complete_x_write_ram = complete_x_write_fifo = complete_x_write_mem

#\textbf{x$\_$read_reg} $<register> [-r <output format>]$
  read_reg_parser = argparse.ArgumentParser()
  read_reg_parser.add_argument('register', type= int, 
    help= 'Use this command to read an specific register from the Comblock.')
  read_reg_parser.add_argument('-r', '--radix', type=str, metavar = 'd', 
  help='Indicate the radix to be used when showing the value read. d = dec, o = oct, b= bin h= hex ', default='d')
  @cmd2.with_category(CMD_CAT_CMB_READ)
  @cmd2.with_argparser(read_reg_parser)
  def do_x_read_reg(self,args):
    """The x_read_reg command to allow reading registers from the Comblock
    """
    if not self.s:
      self.perror("Missing board connection, open the socket with the <connect> command.")
      return
    
    outf= args.radix
    self.poutput("Address: %s" % (args.register))
    self.poutput("format   %s" % (outf))
    tx_buf  = pack("<II",READ_REG,args.register)   #first data is package type
    self.s.send(tx_buf)
    recv_buf = self.s.recv(8)
    rx_dat = unpack('<' + 'I' * (len(recv_buf) // 4), recv_buf)
    self.last_result = rx_dat
    if rx_dat[0] == 1 :
      if outf.lower() == 'b':
        rx_dat = bin(rx_dat[1])
      elif outf.lower() == 'o':
        rx_dat = oct(rx_dat[1])
      elif outf.lower() == 'd':
        rx_dat = rx_dat[1]
      elif outf.lower() == 'h':
        rx_dat = hex(rx_dat[1])
      else: 
        self.perror('Unrecognized radix type use b, o, d, h.')
        return
      self.poutput(rx_dat)
    elif rx_dat[0] == 2:
      self.perror("Unavailable resource, check Comblock configuration.")
      return
    elif rx_dat[0] == 0:
      self.perror("Unavailable resource, check Comblock configuration.")
      return

#\textbf{x$\_$read_ram} $<addr> <N> <inc> [-r <output format>] [-f <filename>]$
  read_ram_parser = argparse.ArgumentParser()
  read_ram_parser.add_argument('addr', type= int, help= 'Address to read from.')
  read_ram_parser.add_argument('N', type= int, help= 'Number of words to read.')
  read_ram_parser.add_argument('inc', type= int, help= 'Incremet step to use when reading several words.')
  read_ram_parser.add_argument('-r', '--radix', type=str, metavar = 'd', 
    help='Indicate the radix to be used when showing the value read. d = dec, o = oct, b= bin h= hex ', default='d')
  read_ram_parser.add_argument('-f', '--file', type= str, default='',
    metavar='filename', help='Output filename.')
  @cmd2.with_category(CMD_CAT_CMB_READ)
  @cmd2.with_argparser(read_ram_parser)
  def do_x_read_ram(self, args):
    """The x_read_ram command to allow reading the RAM of the Comblock
    """
    if not self.s:
      self.perror("Missing board connection, open the socket with the <connect> command.")
      return
    outf = args.radix
    self.poutput("N: %s" % (args.N))
    tx_buf = pack("<IIII", READ_RAM, args.addr, args.N, args.inc)   #first data is package type
    self.s.send(tx_buf)
    rx_buf = self.s.recv((args.N + 1) * 4)
    rx_dat = unpack('<'+'I'*((len(rx_buf) + 1)//4), rx_buf)
    self.last_result = rx_dat
    if rx_dat[0] == 1:
      if outf.lower() == 'b':
        rx_dat = [bin(x) for x in rx_dat[1:]]
      elif outf.lower() == 'o':
        rx_dat = [oct(x) for x in rx_dat[1:]]
      elif outf.lower() == 'h':
        rx_dat = [hex(x) for x in rx_dat[1:]]
      elif outf.lower() == 'd':
        rx_dat = rx_dat[1:]
      else: 
        self.perror('Unrecognized radix type use b, o, d, h.')
        return
      if args.file.strip():
        with open(args.file, 'a') as output:
          output.write(str(rx_dat))
      else:
        self.poutput(rx_dat)
    elif rx_dat[0] == 2:
      self.perror("Unavailable resource, check Comblock configuration.")
      return
    elif rx_dat[0] == 0:
      self.perror("Unavailable resource, check Comblock configuration.")
      return

#\textbf{x$\_$read_mem} $<addr> <N> <inc> [-r <output format>] [-f <filename>]$
  read_mem_parser = argparse.ArgumentParser()
  read_mem_parser.add_argument('addr', type= int, help= 'Address to read from.')
  read_mem_parser.add_argument('N', type= int, help= 'Number of words to read.')
  read_mem_parser.add_argument('inc', type= int, help= 'Incremet step to use when reading several words.')
  read_mem_parser.add_argument('-r', '--radix', type=str, metavar = 'd', 
    help='Indicate the radix to be used when showing the value read. d = dec, o = oct, b= bin h= hex ', default='d')
  read_mem_parser.add_argument('-f', '--file', type= str, default='',
    metavar='filename', help='Output filename.')
  @cmd2.with_category(CMD_CAT_CMB_READ)
  @cmd2.with_argparser(read_mem_parser)
  def do_x_read_mem(self, args):
    """The x_read_mem command to allow reading the memory of the SoC
    Note: This is allows complete access to the memory 
    """
    if not self.s:
      self.perror("Missing board connection, open the socket with the <connect> command.")
      return
    outf = args.radix
    self.poutput("N: %s" % (args.N))
    tx_buf = pack("<IIII", READ_MEM, args.addr, args.N, args.inc)   #first data is package type
    self.s.send(tx_buf)
    rx_buf = self.s.recv((args.N + 1) * 4)
    rx_dat = unpack('<'+'I'*((len(rx_buf) + 1)//4), rx_buf)
    self.last_result = rx_dat
    if rx_dat[0] == 1 :
      if outf.lower() == 'b':
        rx_dat = [bin(x) for x in rx_dat[1:]]
      elif outf.lower() == 'h':
        rx_dat = [hex(x) for x in rx_dat[1:]]
      elif outf.lower() == 'o':
        rx_dat = [oct(x) for x in rx_dat[1:]]
      elif outf.lower() == 'd':
        rx_dat = rx_dat[1:]
      else: 
        self.perror('Unrecognized radix type use b, o, d, h.')
        return
      if args.file.strip():
        with open(args.file, 'a') as output:
          output.write(str(rx_dat))
      else:
        self.poutput(rx_dat)
    elif rx_dat[0] == 2:
      self.perror("Unavailable resource, check Comblock configuration.")
      return
    elif rx_dat[0] == 0:
      self.perror("Unavailable resource, check Comblock configuration.")
      return

#\textbf{x$\_$read_fifo} $<N> [-r <output format>] [-f filename]$
  read_fifo_parser = argparse.ArgumentParser()
  read_fifo_parser.add_argument('N', type= int, 
    help='Number of words to read if 0 will read until FIFO empty.')
  read_fifo_parser.add_argument('-r', '--radix', type= str, metavar = 'd', 
    help='Indicate the radix to be used when showing the value read. d = dec, o = oct, b= bin h= hex ', default='d')
  read_fifo_parser.add_argument('-f', '--file', type= str, default='',
    metavar='filename', help='Output filename.')
  @cmd2.with_category(CMD_CAT_CMB_READ)
  @cmd2.with_argparser(read_fifo_parser)
  def do_x_read_fifo(self,args):
    """The x_read_fifo command to allow reading the FIFO of the Comblock
    """
    if not self.s:
      self.perror("Missing board connection, open the socket with the <connect> command.")
      return
    outf = args.radix
    self.poutput("N: %s" % (args.N))
    tx_buf = pack("<II", READ_FIFO, args.N)   #first data is package type
    self.s.send(tx_buf)
    rx_buf = self.s.recv(1024 * 4)
    rx_dat = unpack('<' + 'I'*((len(rx_buf) + 1)//4), rx_buf)
    self.last_result = rx_dat
    if rx_dat[0] != 2 and rx_dat[0]!= 0:
      if outf.lower() == 'b':
        rx_dat = [bin(x) for x in rx_dat[1:]]
      elif outf.lower() == 'h':
        rx_dat = [hex(x) for x in rx_dat[1:]]
      elif outf.lower() == 'o':
        rx_dat = [oct(x) for x in rx_dat[1:]]
      elif outf.lower() == 'd':
        rx_dat = rx_dat[1:]
      else: 
        self.perror('Unrecognized radix type use b, o, d, h.')
        return
      if args.file.strip():
        with open(args.file, 'a') as output:
          output.write(str(rx_dat))
      else:
        self.poutput(rx_dat)
    elif rx_dat[0] == 2:
      self.perror("Unavailable resource, check Comblock configuration.")
      return
    elif rx_dat[0] == 0:
      self.perror("Unavailable resource, check Comblock configuration.")
      return
    

#\textbf{x$\_$write_reg} $<register>  <data> [-r <output format>]$
  write_reg_parser = argparse.ArgumentParser()
  write_reg_parser.add_argument('register', type= int, 
    metavar= 'register',
    help= 'Register to write to 0-15.')
  write_reg_parser.add_argument('data', type= str,
    metavar= 'data')
  write_reg_parser.add_argument('-r', '--radix', type=str, metavar = 'd', 
  help='Indicate the radix to be used when showing the value read. d = dec, o = oct, b= bin h= hex ', default='d')
  @cmd2.with_category(CMD_CAT_CMB_WRITE)
  @cmd2.with_argparser(write_reg_parser)
  def do_x_write_reg(self,args):
    """The x_write_reg command to allow writing the registers of the Comblock
    """
    if not self.s:
      self.perror("Missing board connection, open the socket with the <connect> command.")
      return
    outf = args.radix
    if outf.lower() == 'b':
      data = int(args.data, 2)
    elif outf.lower() == 'o':
      data = int(args.data, 8)
    elif outf.lower() == 'd':
      data = int(args.data, 10)
    elif outf.lower() == 'h':
      data = int(args.data, 16)
    else: 
      self.perror('Unrecognized radix type use b, o, d, h.')
      return
    tx_buf  = pack("<III", WRITE_REG, args.register, data)  #first data is package type
    self.poutput(tx_buf)
    self.s.send(tx_buf)
    rx_dat = unpack('<I', self.s.recv(4))
    if rx_dat[0] == 1 :
      self.last_result = rx_dat
      return
    elif rx_dat[0] == 2:
      self.perror("Unavailable resource, check Comblock configuration.")
      return 
    elif rx_dat[0] == 0:
      self.perror("Unavailable resource, check Comblock configuration.")
      return 
## Change Active Comblock
#\textbf{x$\_$change_cb} $<comblock> [-r <output format>]$
  change_cb_parser = argparse.ArgumentParser()
  change_cb_parser.add_argument('comblock', type= int, 
    help= 'Use this command to change active Comblock, default is 0.')
  change_cb_parser.add_argument('-r', '--radix', type=str, metavar = 'd', 
  help='Indicate the radix to be used when showing the value read. d = dec, o = oct, b= bin h= hex ', default='d')
  @cmd2.with_category(CMD_CAT_COMMUNICATION)
  @cmd2.with_argparser(change_cb_parser)
  def do_x_change_cb(self,args):
    """The x_change_cb command to change active Comblock in multiple CB designs
    """
    if not self.s:
      self.perror("Missing board connection, open the socket with the <connect> command.")
      return
    
    outf= args.radix
    self.poutput("CB_ID: %s" % (args.comblock))
    self.poutput("format   %s" % (outf))
    tx_buf  = pack("<II",CHANGE_CB,args.comblock)   #first data is package type
    self.s.send(tx_buf)
    rx_dat = unpack('<I', self.s.recv(4))
    if rx_dat[0] == 1 :
      self.last_result = rx_dat
      return
    elif rx_dat[0] == 2:
      self.perror("Unavailable resource, check Comblock configuration.")
      return 
    elif rx_dat[0] == 0:
      self.perror("Unavailable resource, check Comblock configuration.")
      return 

## Start of functions specifics for the pAmp.
## ADC500_CFG 
## @param data Configuration data
## returns status 

#\textbf{x$\_$ADC500_CFG} $<register>  <data> [-r <output format>]$
  adc500_cfg_parser = argparse.ArgumentParser()
  adc500_cfg_parser.add_argument('data', type= str,
    metavar= 'Configuration data')
  adc500_cfg_parser.add_argument('-r', '--radix', type=str, metavar = 'd', 
  help='Indicate the radix to be used when showing the value read. d = dec, o = oct, b= bin h= hex ', default='d')
  @cmd2.with_category(CMD_CAT_PAMP)
  @cmd2.with_argparser(adc500_cfg_parser)
  def do_adc500_cfg(self,args):
    """The ADC500_CFG command allow writing in the ADC500  configuration register
    """
    if not self.s:
      self.perror("Missing board connection, open the socket with the <connect> command.")
      return
    outf = args.radix
    if outf.lower() == 'b':
      data = int(args.data, 2)
    elif outf.lower() == 'o':
      data = int(args.data, 8)
    elif outf.lower() == 'd':
      data = int(args.data, 10)
    elif outf.lower() == 'h':
      data = int(args.data, 16)
    else: 
      self.perror('Unrecognized radix type use b, o, d, h.')
      return
    tx_buf  = pack("<II", ADC500_CFG, data)  #first data is package type
    self.poutput(tx_buf)
    self.s.send(tx_buf)
    rx_dat = unpack('<II', self.s.recv(8))
    if rx_dat[0] == 1 :
      self.last_result = rx_dat
      return
    elif rx_dat[0] == 2:
      self.perror("Unavailable resource, check Comblock configuration.")
      return 
    elif rx_dat[0] == 0:
      self.perror("Unavailable resource, check Comblock configuration.")
      return 
  
## SET_DEC  
## @param N: Decimation value 
## returns N to acknowledge the input value. 

#\textbf{x$\_$set_dec_n} $<register>  <N> [-r <output format>]$
  set_dec_n_parser = argparse.ArgumentParser()
  set_dec_n_parser.add_argument('N', type= str,
    metavar= 'Set N')
  set_dec_n_parser.add_argument('-r', '--radix', type=str, metavar = 'd', 
  help='Indicate the radix to be used when showing the value read. d = dec, o = oct, b= bin h= hex ', default='d')
  @cmd2.with_category(CMD_CAT_PAMP)
  @cmd2.with_argparser(set_dec_n_parser)
  def do_set_dec_n(self,args):
    """The SET_DEC_N command allows to set a decimation value. 
    """
    if not self.s:
      self.perror("Missing board connection, open the socket with the <connect> command.")
      return
    outf = args.radix
    if outf.lower() == 'b':
      N = int(args.N, 2)
    elif outf.lower() == 'o':
      N = int(args.N, 8)
    elif outf.lower() == 'd':
      N = int(args.N, 10)
    elif outf.lower() == 'h':
      N = int(args.N, 16)
    else: 
      self.perror('Unrecognized radix type use b, o, d, h.')
      return
    tx_buf  = pack("<II", SET_DEC_N, N) 
    self.poutput(tx_buf)
    self.s.send(tx_buf)
    rx_dat = unpack('<II', self.s.recv(8))
    if rx_dat[0] == 1 :
      self.last_result = rx_dat
      return
    elif rx_dat[0] == 2:
      self.perror("Unavailable resource, check Comblock configuration.")
      return 
    elif rx_dat[0] == 0:
      self.perror("Unavailable resource, check Comblock configuration.")
      return 

## pAmp_INIT  
## Function to initialize the PicoAmmeter 
## @param none
## returns status 

  #\textbf{x$\_$pAmp_init} $<register>  <data> [-r <output format>]$
  pamp_init_parser = argparse.ArgumentParser()
  @cmd2.with_category(CMD_CAT_PAMP)
  @cmd2.with_argparser(pamp_init_parser)
  def do_pAmp_init(self,args):
    """Function to initialize the PicoAmmeter
    """
    if not self.s:
      self.perror("Missing board connection, open the socket with the <connect> command.")
      return
    tx_buf  = pack("<I", PAMP_INIT)  #send PAMP_INIT command
    self.poutput(tx_buf)
    self.s.send(tx_buf)
    rx_dat = unpack('<II', self.s.recv(8))
    if rx_dat[0] == 1 :
      self.last_result = rx_dat
      return
    elif rx_dat[0] == 2:
      self.perror("Unavailable resource, check Comblock configuration.")
      return 
    elif rx_dat[0] == 0:
      self.perror("Unavailable resource, check Comblock configuration.")
      return 

## VADJ_SET  
## Set the value of the VADJ on the CIAA
## @param VADJ from 0 to 127
## returns VADJ set value 

  #\textbf{x$\_$pAmp_init} $<register>  <data> [-r <output format>]$
  vadj_set_parser = argparse.ArgumentParser()
  vadj_set_parser.add_argument('VADJ', type= str,
    metavar= 'Set VADJ value from 0 to 127')
  vadj_set_parser.add_argument('-r', '--radix', type=str, metavar = 'd', 
  help='Indicate the radix to be used when showing the value read. d = dec, o = oct, b= bin h= hex ', default='d')
  @cmd2.with_category(CMD_CAT_PAMP)
  @cmd2.with_argparser(vadj_set_parser)
  def do_vadj_set(self,args):
    """Function to initialize the PicoAmmeter
    """
    if not self.s:
      self.perror("Missing board connection, open the socket with the <connect> command.")
      return
    outf = args.radix
    if outf.lower() == 'b':
      VADJ = int(args.VADJ, 2)
    elif outf.lower() == 'o':
      VADJ = int(args.VADJ, 8)
    elif outf.lower() == 'd':
      VADJ = int(args.VADJ, 10)
    elif outf.lower() == 'h':
      VADJ = int(args.VADJ, 16)
    else: 
      self.perror('Unrecognized radix type use b, o, d, h.')
      return
    tx_buf  = pack("<II", VADJ_SET, VADJ)  #first data is package type
    self.poutput(tx_buf)
    self.s.send(tx_buf)
    rx_dat = unpack('<II', self.s.recv(8))
    if rx_dat[0] == 1 :
      self.last_result = rx_dat
      return
    elif rx_dat[0] == 2:
      self.perror("Unavailable resource, check Comblock configuration.")
      return 
    elif rx_dat[0] == 0:
      self.perror("Unavailable resource, check Comblock configuration.")
      return 

## PAS_SET  
## Set the value of the Picoammeter static control
## @param cfg_reg from 0 to 127
## returns status set value 

  #\textbf{x$\_$pAmp_init} $<register>  <data> [-r <output format>]$
  pas_set_parser = argparse.ArgumentParser()
  pas_set_parser.add_argument('cfg_reg', type= str,
    metavar= 'Set config register value')
  pas_set_parser.add_argument('-r', '--radix', type=str, metavar = 'd', 
  help='Indicate the radix to be used when showing the value read. d = dec, o = oct, b= bin h= hex ', default='d')
  @cmd2.with_category(CMD_CAT_PAMP)
  @cmd2.with_argparser(pas_set_parser)
  def do_pas_set(self,args):
    """Function to set configuration on the PicoAmmeter
    """
    if not self.s:
      self.perror("Missing board connection, open the socket with the <connect> command.")
      return
    outf = args.radix
    if outf.lower() == 'b':
      cfg_reg = int(args.cfg_reg, 2)
    elif outf.lower() == 'o':
      cfg_reg = int(args.cfg_reg, 8)
    elif outf.lower() == 'd':
      cfg_reg = int(args.cfg_reg, 10)
    elif outf.lower() == 'h':
      cfg_reg = int(args.cfg_reg, 16)
    else: 
      self.perror('Unrecognized radix type use b, o, d, h.')
      return
    tx_buf  = pack("<II", PAS_SET, cfg_reg)  #first data is package type
    self.poutput(tx_buf)
    self.s.send(tx_buf)
    rx_dat = unpack('<II', self.s.recv(8))
    if rx_dat[0] == 1 :
      self.last_result = rx_dat
      return
    elif rx_dat[0] == 2:
      self.perror("Unavailable resource, check Comblock configuration.")
      return 
    elif rx_dat[0] == 0:
      self.perror("Unavailable resource, check Comblock configuration.")
      return 


#\textbf{x$\_$write_ram} $<addr> (<data> | -f) <N> <inc> [-r <output format>]$
  write_ram_parser = argparse.ArgumentParser()
  write_ram_parser.add_argument('addr', type= int, help= 'Absolute address to write to, ignored when reading from file.')
  write_ram_parser.add_argument('data', type= pdata,
    help= 'Comma separeted data accepted 1,2,3 or single data 1 or filepath.')
  write_ram_parser.add_argument('N', type= int, help= 'Number of words to write. Ignored when reading from file.')
  write_ram_parser.add_argument('inc', type= int, help= 'Increment step to the address when writing. Ignored when reading from file.') 
  write_ram_parser.add_argument('-r', '--radix', type=str, metavar = 'd', 
    help='Indicate the radix to be used when interpreting the value.', default='d')
  @cmd2.with_category(CMD_CAT_CMB_WRITE)
  @cmd2.with_argparser(write_ram_parser)
  def do_x_write_ram(self,args):
    """The x_write_ram command to allow writing the RAM of the Comblock
    """
    if not self.s:
      self.perror("Missing board connection, open the socket with the <connect> command.")
      return
    outf = args.radix
    if outf.lower() == 'b':
      base = 2
    elif outf.lower() == 'o':
      base = 8
    elif outf.lower() == 'd':
      base = 10
    elif outf.lower() == 'h':
      base = 16
    else: 
      self.perror('Unrecognized radix type use b, o, d, h.')
      return
    if not args.data[0]: #file
      filename = args.data[1]
      filesize = getsize(filename)
      if (filesize == 0):
        self.perror("Empty file, nothing to read.")
        return
      else:
        progress = tqdm.tqdm(range(filesize), f"Sending {filename}")
        with open(filename, "r") as f:
          self.poutput(f"Sending {filename}")
          for _ in progress:
            line = f.readline().rstrip()
            if line[0] != '#': # ignore if comment or header
              try:
                addr, data = int(line.split(',')[0], base), int(line.split(',')[1], base)
                tx_buf  = pack("<IIIIII", WRITE_RAM, addr, 1, 1, ord(args.radix), data)
                self.s.send(tx_buf)
                progress.update(1)
                rx_dat = unpack('<I', self.s.recv(4))
                self.last_result = rx_dat
                if not rx_dat[0]:
                  self.perror("Unavailable resource, check Comblock configuration.")
                  return
                elif rx_dat[0] == 2:
                  self.perror("Overflow condition active.")
                  return
                else:
                  pass
              except Exception as error:
                self.perror(error)
                return
          #first data is package type
    else:
      tx_buf  = pack("<IIIII", WRITE_RAM, args.addr, args.N, args.inc, ord(args.radix)) \
        + pack('I'*args.N,  *[int(d, base) for d in args.data[1]])
         #first data is package type
      self.s.send(tx_buf)
      rx_dat = unpack('<I', self.s.recv(4))
      self.last_result = rx_dat
      if not rx_dat[0]:
        self.perror("Unavailable resource, check Comblock configuration.")
        return
      elif rx_dat[0] == 2:
        self.perror("Overflow condition active.")
        return


#\textbf{x$\_$write_mem} $<addr> (<data> | -f) <N> <inc> [-r <output format>]$
  write_mem_parser = argparse.ArgumentParser()
  write_mem_parser.add_argument('addr', type= int, help= 'Absolute address to write to, ignored when reading from file.')
  write_mem_parser.add_argument('data', type= pdata,
    help= 'Comma separeted data accepted 1,2,3 or single data 1 or file.')
  write_mem_parser.add_argument('N', type= int, help= 'Number of words to write. Ignored when reading from file.')
  write_mem_parser.add_argument('inc', type= int, help= 'Increment step to the address when writing. Ignored when reading from file.') 
  write_mem_parser.add_argument('-r', '--radix', type=str, metavar = 'd', 
    help='Indicate the radix to be used when interpreting the value.', default='d')
  @cmd2.with_category(CMD_CAT_CMB_WRITE)
  @cmd2.with_argparser(write_mem_parser)
  def do_x_write_mem(self,args):
    """The x_write_mem command to allow writing the memory of the SoC
      Note: This function allows access to the whole addressable memory use with caution.
    """
    if not self.s:
      self.perror("Missing board connection, open the socket with the <connect> command.")
      return
    outf = args.radix
    if outf.lower() == 'b':
      base = 2
    elif outf.lower() == 'o':
      base = 8
    elif outf.lower() == 'd':
      base = 10
    elif outf.lower() == 'h':
      base = 16
    else: 
      self.perror('Unrecognized radix type use b, o, d, h.')
      return
    if not args.data[0]: #file
      filename = args.data[1]
      filesize = getsize(filename)
      if (filesize == 0):
        self.perror("Empty file, nothing to read.")
        return
      else:
        progress = tqdm.tqdm(range(filesize), f"Sending {filename}")
        with open(filename, "r") as f:
          self.poutput(f"Sending {filename}")
          for _ in progress:
            line = f.readline().rstrip()
            if line[0] != '#': # ignore if comment or header
              try:
                addr, data = int(line.split(',')[0], base), int(line.split(',')[1], base)
                tx_buf  = pack("<IIIIII", WRITE_MEM, addr, 1, 1, ord(args.radix), data)
                self.s.send(tx_buf)
                progress.update(1)
                rx_dat = unpack('<I', self.s.recv(4))
                self.last_result = rx_dat
                if not rx_dat[0]:
                  self.perror("Unavailable resource, check Comblock configuration.")
                  return
                elif rx_dat[0] == 2:
                  self.perror("Overflow condition active.")
                  return
                else:
                  pass
              except Exception as error:
                self.perror(error)
                return
          #first data is package type
    else:
      tx_buf  = pack("<IIIII", WRITE_MEM, args.addr, args.N, args.inc, ord(args.radix)) \
        + pack('I'*args.N,  *[int(d, base) for d in args.data[1]])
         #first data is package type
      self.s.send(tx_buf)
      rx_dat = unpack('<I', self.s.recv(4))
      self.last_result = rx_dat
      if not rx_dat[0]:
        self.perror("Unavailable resource, check Comblock configuration.")
        return
      elif rx_dat[0] == 2:
        self.perror("Overflow condition active.")
        return
    
#\textbf{x$\_$write_fifo} $<data> <N> [-r <output format>]$
  write_fifo_parser = argparse.ArgumentParser()
  write_fifo_parser.add_argument('data', type= pdata,
    help= 'Comma separeted data accepted 1,2,3 or single data 1 or file.')
  write_fifo_parser.add_argument('N', type= int, metavar= 'N', help='Number of words to write')
  write_fifo_parser.add_argument('-r', '--radix', type=str, metavar = 'd', 
    help='Indicate the radix to be used when interpreting the value.', default='d')
  @cmd2.with_category(CMD_CAT_CMB_WRITE)
  @cmd2.with_argparser(write_fifo_parser)
  def do_x_write_fifo(self,args):
    """The x_write_fifo command to allow writing the FIFO of the Comblock
    """
    if not self.s:
      self.perror("Missing board connection, open the socket with the <connect> command.")
      return
    outf = args.radix
    if outf.lower() == 'b':
      base = 2
    elif outf.lower() == 'o':
      base = 8
    elif outf.lower() == 'd':
      base = 10
    elif outf.lower() == 'h':
      base = 16
    else: 
      self.perror('Unrecognized radix type use b, o, d, h.')
      return
    if not args.data[0]: #file
      filename = args.data[1]
      filesize = getsize(filename)
      if (filesize == 0):
        self.perror("Empty file, nothing to read.")
        return
      else:
        progress = tqdm.tqdm(range(filesize), f"Sending {filename}")
        with open(filename, "r") as f:
          self.poutput(f"Sending {filename}")
          for _ in progress:
            line = f.readline().rstrip()
            if line[0] != '#': # ignore if comment or header
              try:
                addr, data = int(line.split(',')[0], base), int(line.split(',')[1], base)
                tx_buf  = pack("<III", WRITE_FIFO, 1, data)
                self.s.send(tx_buf)
                progress.update(1)
                rx_dat = unpack('<I', self.s.recv(4))
                self.last_result = rx_dat
                if not rx_dat[0]:
                  self.perror("Unavailable resource, check Comblock configuration.")
                  return
                elif rx_dat[0] == 2:
                  self.perror("Overflow condition active.")
                  return
                else:
                  pass
              except Exception as error:
                self.perror(error)
                return
          #first data is package type
    else:
      tx_buf  = pack("<II", WRITE_FIFO, args.N) + pack('I'*args.N, *[int(d, base) for d in args.data[1]])
      self.s.send(tx_buf)
      rx_dat = unpack('<I', self.s.recv(4))
      self.last_result = rx_dat
      if not rx_dat[0]:
        self.perror("Unavailable resource, check Comblock configuration.")
        return
      elif rx_dat[0] == 2:
        self.perror("Overflow condition active.")
        return 

## HV_SET  
## Set the value of the HV POWER SUPPLY 
## @param HVADJ from 0 to 65535
## returns HVADJ set value 

  #\textbf{x$\_$hv_set} $<register>  <data> [-r <output format>]$
  HVadj_set_parser = argparse.ArgumentParser()
  HVadj_set_parser.add_argument('HVADJ', type= str,
    metavar= 'Set HVADJ value from 0 to 65535')
  HVadj_set_parser.add_argument('-r', '--radix', type=str, metavar = 'd', 
  help='Indicate the radix to be used when showing the value read. d = dec, o = oct, b= bin h= hex ', default='d')
  @cmd2.with_category(CMD_CAT_PAMP)
  @cmd2.with_argparser(HVadj_set_parser)
  def do_HVadj_set(self,args):
    """Function to set the HV power supply
    """
    if not self.s:
      self.perror("Missing board connection, open the socket with the <connect> command.")
      return
    outf = args.radix
    if outf.lower() == 'b':
      HVADJ = int(args.HVADJ, 2)
    elif outf.lower() == 'o':
      HVADJ = int(args.HVADJ, 8)
    elif outf.lower() == 'd':
      HVADJ = int(args.HVADJ, 10)
    elif outf.lower() == 'h':
      HVADJ = int(args.HVADJ, 16)
    else: 
      self.perror('Unrecognized radix type use b, o, d, h.')
      return
    tx_buf  = pack("<II", HV_SET, HVADJ)  #first data is package type
    self.poutput(tx_buf)
    self.s.send(tx_buf)
    rx_dat = unpack('<II', self.s.recv(8))
    if rx_dat[0] == 1 :
      self.last_result = rx_dat
      return
    elif rx_dat[0] == 2:
      self.perror("Unavailable resource, check Comblock configuration.")
      return 
    elif rx_dat[0] == 0:
      self.perror("Unavailable resource, check Comblock configuration.")
      return 

## HV_HVSET_RAMP
## returns HVADJ set value 

  #\textbf{x$\_$hv_set_ramp} <Vinit> <Vend> <t> [-r <output format>]$
  HVadj_set_ramp_parser = argparse.ArgumentParser()
  HVadj_set_ramp_parser.add_argument('Vinit', type=int,
    metavar='Vinit',
    help='Initial value of ramp')
  HVadj_set_ramp_parser.add_argument('Vend', type=int,
    metavar='Vend',
    help='End value of ramp,')
  HVadj_set_ramp_parser.add_argument('t', type=int,
    metavar='t',
    help='Time in seconds to ramp value',
    default=10)
  @cmd2.with_category(CMD_CAT_PAMP)
  @cmd2.with_argparser(HVadj_set_ramp_parser)
  def do_HVadj_set_ramp(self,args):
    """Function to set the HV power supply
    """
    if not self.s:
      self.perror("Missing board connection, open the socket with the <connect> command.")
      return

    hvdac_vinit=(args.Vinit/2000)*(2**16-1)
    hvdac_vend=(args.Vinit/2000)*(2**16-1)
    ramp=np.linspace(hvdac_vinit, hvdac_vend,args.t*100) #time steps of 100 ms 
    print("Ramping voltage from %d, to %d \n", args.Vinit, args.Vend)
    print("Ramping")

    for HVADJ in ramp:
      tx_buf  = pack("<II", HV_SET, int(HVADJ))  #first data is package type
      # self.poutput(tx_buf)
      self.s.send(tx_buf)
      rx_dat = unpack('<II', self.s.recv(8))
      if rx_dat[0] == 1 :
        self.last_result = rx_dat
        sleep(0.01)
      elif rx_dat[0] == 2:
        self.perror("Unavailable resource, check Comblock configuration.")
        return 
      elif rx_dat[0] == 0:
        self.perror("Unavailable resource, check Comblock configuration.")
        return 
    self.poutput("Done")



## HV_PWRDWN  
## POWER DOWN HV POWER SUPPLY 
## @param PWR 
## returns PWR set value on register

  #\textbf{x$\_$hv_set} $<register>  <data> [-r <output format>]$
  hv_pwrdwn_parser = argparse.ArgumentParser()
  hv_pwrdwn_parser.add_argument('PWR', type= str,
    metavar= 'Set PWR value from 0 to 3')
  hv_pwrdwn_parser.add_argument('-r', '--radix', type=str, metavar = 'd', 
  help='Indicate the radix to be used when showing the value read. d = dec, o = oct, b= bin h= hex ', default='d')
  @cmd2.with_category(CMD_CAT_PAMP)
  @cmd2.with_argparser(hv_pwrdwn_parser)
  def do_hv_pwrdwn(self,args):
    """Function to initialize the HV PWRDWN
    """
    if not self.s:
      self.perror("Missing board connection, open the socket with the <connect> command.")
      return
    outf = args.radix
    if outf.lower() == 'b':
      PWR = int(args.PWR, 2)
    elif outf.lower() == 'o':
      PWR = int(args.PWR, 8)
    elif outf.lower() == 'd':
      PWR = int(args.PWR, 10)
    elif outf.lower() == 'h':
      PWR = int(args.PWR, 16)
    else: 
      self.perror('Unrecognized radix type use b, o, d, h.')
      return
    tx_buf  = pack("<II", HV_PWRDOWN, PWR)  #first data is package type
    self.poutput(tx_buf)
    self.s.send(tx_buf)
    rx_dat = unpack('<II', self.s.recv(8))
    if rx_dat[0] == 1 :
      self.last_result = rx_dat
      return
    elif rx_dat[0] == 2:
      self.perror("Unavailable resource, check Comblock configuration.")
      return 
    elif rx_dat[0] == 0:
      self.perror("Unavailable resource, check Comblock configuration.")
      return 

#VMON READ
#Read VMON from HV DC-DC Converter board

#\textbf{x$\_$vmon_read} $<none> [-r <output format>]$
  vmon_read_parser = argparse.ArgumentParser()
  vmon_read_parser.add_argument('-r', '--radix', type=str, metavar = 'd', 
  help='Indicate the radix to be used when showing the value read. d = dec, o = oct, b= bin h= hex ', default='d')
  @cmd2.with_category(CMD_CAT_PAMP)
  @cmd2.with_argparser(vmon_read_parser)
  def do_vmon_read(self,args):
    """The vmon_read command allows reading vmon from the HV DC
    """
    if not self.s:
      self.perror("Missing board connection, open the socket with the <connect> command.")
      return
    
    outf= args.radix
    self.poutput("format   %s" % (outf))
    tx_buf  = pack("<I",VMON_READ)   #first data is package type
    self.s.send(tx_buf)
    recv_buf = self.s.recv(8)
    rx_dat = unpack('<' + 'I' * (len(recv_buf) // 4), recv_buf)
    self.last_result = rx_dat
    if rx_dat[0] == 1 :
      if outf.lower() == 'b':
        rx_dat = bin(rx_dat[1])
      elif outf.lower() == 'o':
        rx_dat = oct(rx_dat[1])
      elif outf.lower() == 'd':
        rx_dat = rx_dat[1]
      elif outf.lower() == 'h':
        rx_dat = hex(rx_dat[1])
      else: 
        self.perror('Unrecognized radix type use b, o, d, h.')
        return
      self.poutput(rx_dat)
    elif rx_dat[0] == 2:
      self.perror("Unavailable resource, check Comblock configuration.")
      return
    elif rx_dat[0] == 0:
      self.perror("Unavailable resource, check Comblock configuration.")
      return

#IMON READ
#Read VMON from HV DC-DC Converter board

#\textbf{x$\_$imon_read} $<none> [-r <output format>]$
  imon_read_parser = argparse.ArgumentParser()
  imon_read_parser.add_argument('-r', '--radix', type=str, metavar = 'd', 
  help='Indicate the radix to be used when showing the value read. d = dec, o = oct, b= bin h= hex ', default='d')
  @cmd2.with_category(CMD_CAT_PAMP)
  @cmd2.with_argparser(imon_read_parser)
  def do_imon_read(self,args):
    """The imon_read command allows reading vmon from the HV DC
    """
    if not self.s:
      self.perror("Missing board connection, open the socket with the <connect> command.")
      return
    
    outf= args.radix
    self.poutput("format   %s" % (outf))
    tx_buf  = pack("<I",IMON_READ)   #first data is package type
    self.s.send(tx_buf)
    recv_buf = self.s.recv(8)
    rx_dat = unpack('<' + 'I' * (len(recv_buf) // 4), recv_buf)
    self.last_result = rx_dat
    if rx_dat[0] == 1 :
      if outf.lower() == 'b':
        rx_dat = bin(rx_dat[1])
      elif outf.lower() == 'o':
        rx_dat = oct(rx_dat[1])
      elif outf.lower() == 'd':
        rx_dat = rx_dat[1]
      elif outf.lower() == 'h':
        rx_dat = hex(rx_dat[1])
      else: 
        self.perror('Unrecognized radix type use b, o, d, h.')
        return
      self.poutput(rx_dat)
    elif rx_dat[0] == 2:
      self.perror("Unavailable resource, check Comblock configuration.")
      return
    elif rx_dat[0] == 0:
      self.perror("Unavailable resource, check Comblock configuration.")
      return


############ECAL HV #######################################################
#\textbf{x$\_$ecal_set_ch} $<register>  <data> [-r <output format>]$
  ecal_set_ch_parser = argparse.ArgumentParser()
  ecal_set_ch_parser.add_argument('channel', type= int, 
    metavar= 'channel',
    help= 'Channel to write to 0-31.')
  ecal_set_ch_parser.add_argument('data', type= str,
    metavar= 'data')
  ecal_set_ch_parser.add_argument('-r', '--radix', type=str, metavar = 'd', 
  help='Indicate the radix to be used when showing the value read. d = dec, o = oct, b= bin h= hex ', default='d')
  @cmd2.with_category(CMD_CAT_CMB_WRITE)
  @cmd2.with_argparser(ecal_set_ch_parser)
  def do_x_ecal_set_ch(self,args):
    """The ecal_set_ch command to allow writing the value to polarize the ECAL 
    IMPORTANT: be sure to run "pAmp_init" command before.
    """
    if not self.s:
      self.perror("Missing board connection, open the socket with the <connect> command.")
      return
    outf = args.radix
    if outf.lower() == 'b':
      data = int(args.data, 2)
    elif outf.lower() == 'o':
      data = int(args.data, 8)
    elif outf.lower() == 'd':
      data = int(args.data, 10)
    elif outf.lower() == 'h':
      data = int(args.data, 16)
    else: 
      self.perror('Unrecognized radix type use b, o, d, h.')
      return
    tx_buf  = pack("<III", ECAL_SET_CH, args.channel, data)  #first data is package type
    self.poutput(tx_buf)
    self.s.send(tx_buf)
    rx_dat = unpack('<II', self.s.recv(8))
    if rx_dat[0] == 1 :
      self.last_result = rx_dat
      return
    elif rx_dat[0] == 2:
      self.perror("Unavailable resource, check Comblock configuration.")
      return 
    elif rx_dat[0] == 0:
      self.perror("Unavailable resource, check Comblock configuration.")
      return       

############ECAL HV CH RAMP #######################################################
#\textbf{x$\_$ecal_ramp_ch} $ <ch> <Vinit> <Vend> <t(ms)> [-r <output format>]$

  ecal_ramp_ch_parser = argparse.ArgumentParser()
  ecal_ramp_ch_parser.add_argument('channel', type= int, 
    metavar= 'channel',
    help= 'Channel to write to 0-31.')
  ecal_ramp_ch_parser.add_argument('Vinit', type=int,
    metavar='Vinit',
    help='Initial value of ramp')
  ecal_ramp_ch_parser.add_argument('Vend', type=int,
    metavar='Vend',
    help='End value of ramp,')
  ecal_ramp_ch_parser.add_argument('t', type=int,
    metavar='t',
    help='Time in seconds to ramp value',
    default=10)
  @cmd2.with_category(CMD_CAT_CMB_WRITE)
  @cmd2.with_argparser(ecal_ramp_ch_parser)
  def do_x_ecal_ramp_ch(self,args):
    """The ecal_ramp_ch command to allow writing the value to polarize the ECAL 
    IMPORTANT: be sure to run "pAmp_init" command before.
    """
    if not self.s:
      self.perror("Missing board connection, open the socket with the <connect> command.")
      return

    ##16 bits resolution for 100 V reference.
    dac_init=args.Vinit*(2**12/100-1)
    dac_end=args.Vend*(2**12/100-1)
    ramp=np.linspace(dac_init, dac_end,args.t*100) #time steps of 10 ms 
    print("Ramping voltage from %d, to %d \n", args.Vinit, args.Vend)
    print("Ramping")

    for data in ramp:    
      # print(int(data)*100/2**12)
      tx_buf  = pack("<III", ECAL_SET_CH, args.channel, int(data))  #first data is package type
      # self.poutput(tx_buf)
      self.s.send(tx_buf)
      rx_dat = unpack('<II', self.s.recv(8))
      if rx_dat[0] == 1 :
        # self.last_result = rx_dat
        sleep(0.01)   
        # print(".")
      elif rx_dat[0] == 2:
        self.perror("Unavailable resource, check Comblock configuration.")
        return 
      elif rx_dat[0] == 0:
        self.perror("Unavailable resource, check Comblock configuration.")
        return    
    print("Done")


############ECAL HV ALL RAMP #######################################################
#\textbf{x$\_$ecal_ramp_all} $ <Vinit> <Vend> <t(s)> [-r <output format>]$

  ecal_ramp_all_parser = argparse.ArgumentParser()
  ecal_ramp_all_parser.add_argument('Vinit', type=int,
    metavar='Vinit',
    help='Initial value of ramp')
  ecal_ramp_all_parser.add_argument('Vend', type=int,
    metavar='Vend',
    help='End value of ramp,')
  ecal_ramp_all_parser.add_argument('t', type=int,
    metavar='t',
    help='Time in seconds to ramp value',
    default=10)
  @cmd2.with_category(CMD_CAT_CMB_WRITE)
  @cmd2.with_argparser(ecal_ramp_all_parser)
  def do_x_ecal_ramp_all(self,args):
    """The ecal_ramp_all command to allow writing the value to polarize the ECAL 
    IMPORTANT: be sure to run "pAmp_init" command before.
    """
    if not self.s:
      self.perror("Missing board connection, open the socket with the <connect> command.")
      return

    ##16 bits resolution for 100 V reference.
    dac_init=args.Vinit*(2**12/100-1)
    dac_end=args.Vend*(2**12/100-1)
    ramp=np.linspace(dac_init, dac_end,args.t*10) #time steps of 100 ms 
    print("Ramping voltage from %d, to %d \n", args.Vinit, args.Vend)
    print("Ramping")

    for data in ramp:    
      # print(int(data)*100/2**12)
      tx_buf  = pack("<III", ECAL_SET_ALL, 31, int(data))  #first data is package type
      # self.poutput(tx_buf)
      self.s.send(tx_buf)
      rx_dat = unpack('<II', self.s.recv(8))
      if rx_dat[0] == 1 :
        # self.last_result = rx_dat
        sleep(0.1)   
        # print(".")
      elif rx_dat[0] == 2:
        self.perror("Unavailable resource, check Comblock configuration.")
        return 
      elif rx_dat[0] == 0:
        self.perror("Unavailable resource, check Comblock configuration.")
        return    
    print("Done")



############ECAL HV_SET_ALL #######################################################
#\textbf{x$\_$ecal_set_all} $<register>  <data> [-r <output format>]$
  ecal_set_all_parser = argparse.ArgumentParser()
  ecal_set_all_parser.add_argument('N', type= int, 
    metavar= 'N',
    help= 'N number of PMT on the SPI chain.')
  ecal_set_all_parser.add_argument('data', type= str,
    metavar= 'data')
  ecal_set_all_parser.add_argument('-r', '--radix', type=str, metavar = 'd', 
  help='Indicate the radix to be used when showing the value read. d = dec, o = oct, b= bin h= hex ', default='d')
  @cmd2.with_category(CMD_CAT_CMB_WRITE)
  @cmd2.with_argparser(ecal_set_all_parser)
  def do_x_ecal_set_all(self,args):
    """The ecal_set_all command to allow writing the value to polarize the ECAL 
    IMPORTANT: be sure to run "pAmp_init" command before.
    """
    if not self.s:
      self.perror("Missing board connection, open the socket with the <connect> command.")
      return
    outf = args.radix
    if outf.lower() == 'b':
      data = int(args.data, 2)
    elif outf.lower() == 'o':
      data = int(args.data, 8)
    elif outf.lower() == 'd':
      data = int(args.data, 10)
    elif outf.lower() == 'h':
      data = int(args.data, 16)
    else: 
      self.perror('Unrecognized radix type use b, o, d, h.')
      return
    tx_buf  = pack("<III", ECAL_SET_ALL, args.N, data)  #first data is package type
    self.poutput(tx_buf)
    self.s.send(tx_buf)
    rx_dat = unpack('<II', self.s.recv(8))
    if rx_dat[0] == 1 :
      self.last_result = rx_dat
      return
    elif rx_dat[0] == 2:
      self.perror("Unavailable resource, check Comblock configuration.")
      return 
    elif rx_dat[0] == 0:
      self.perror("Unavailable resource, check Comblock configuration.")
      return       



#\textbf{x$\_$udma} $<src_addr> <dst_addr> <src_inc> <dst_inc> <N>$
  udma_parser = argparse.ArgumentParser()
  udma_parser.add_argument('src_addr', type = int, default= 0,
    metavar = 'src_addr',
    help = 'Source address.')
  udma_parser.add_argument('dst_addr', type = int, default= 0,
    metavar = 'dst_addr',
    help = 'Destination address.')
  udma_parser.add_argument('src_inc', type = int, default= 0,
    metavar = 'src_inc',
    help = 'Source address increment step.')
  udma_parser.add_argument('dst_inc', type = int, default= 0,
    metavar = 'dst_inc',
    help = 'Destination address increment step')
  udma_parser.add_argument('N', type = int, default= 0,
    metavar = 'N',
    help = 'Number of words to move.')
  @cmd2.with_category(CMD_CAT_COMMUNICATION)
  @cmd2.with_argparser(udma_parser)
  def do_udma (self, args):
    """The udma command to pass the UDMA instruction to the specified LRA
    Note: As it is this function is not completely implemented and must not be used unless specified in the release notes
    """
    if not self.s:
      self.perror("Missing board connection, open the socket with the <connect> command.")
      return
    tx_buf  = pack("<IIIII", UDMA, *args.udma)  #first data is package type
    self.s.send(tx_buf)
    self.s.settimeout(4)
    try:
      if unpack('<I', self.s.recv(4)):
        self.poutput("Success")
      else:
        self.perror("Unavailable resource, check Comblock configuration.")
    except socket.timeout:
      self.perror("Timeout!!! Something went wrong")
    return 
  
  @cmd2.with_category(CMD_CAT_COMMUNICATION)
  def do_log(self,args):
    """Starts serial logging to debug the transmission and processing of the messages
    """
    if not self.s:
      self.perror("Missing board connection, open the socket with the <connect> command.")
      return
    tx_buf  = pack("<I", LOG)  #first data is package type
    self.s.send(tx_buf)


def main(argv=None) :
  """Entry point for the application

  Parameters
  ----------
  None
  Returns
  -------
  None
  """
  parser = argparse.ArgumentParser()
  command_help = 'optional command to run, if no command given, enter an interactive shell'
  parser.add_argument('command', nargs= '?', help= command_help)
  arg_help = 'optional arguments for command'
  parser.add_argument('command_args', nargs=argparse.REMAINDER, help=arg_help)
  args = parser.parse_args(argv)
  c = UDMA_cli()
  sys_exit_code = 0
  if args.command:
    c.onecmd_plus_hooks('{} {}'.format(args.command, ' '.join(args.command_args)))
  else:
    sys_exit_code = c.cmdloop()
  return sys_exit_code

if __name__ == '__main__':
  sys.exit(main())

