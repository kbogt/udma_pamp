#ifndef UDMA_H
#define UDMA_H

#include <stdio.h>
#include <string.h>
#include "comblock.h"

#include "comblock_configs.h"
#include "pAmp.h"

#define true 1
#define false 0

#include "xparameters.h"

#define READ_REG           0
#define READ_RAM           1
#define READ_MEM           2
#define READ_FIFO          3
/////////////////////////////////////
#define WRITE_REG          4
#define WRITE_RAM          5
#define WRITE_MEM          6
#define WRITE_FIFO         7
///////////////////////////////////
#define UDMA               8
#define CHANGE_CB		   127
#define LOG				   255
#define BUFF_SIZE          4096

///////////////////////////////////
//picoAmmeter Specific functions
#define ADC500_CFG_FUNC		100
#define DEC_CFG				101
#define PAMP_INIT			102
#define VADJ_SET			103
#define PAS_SET				104
#define PAMP_POT			105
#define PAMP_TEMPS			106
#define PAMP_GEN_HISTO		107
#define PAMP_GET_HISTO		108
/////////////////////////////////
#define HISTO_CLEAN			200
#define HISTO_SET			201
#define HISTO_ENABLE		202
#define HISTO_READ			203
#define HISTO_RDCOUNT		204
#define HISTO_ISDONE		205

/*TODO required module registers
ID:0
Size:1
Type:2
	operator
	storage
	router
	interface
MemType:3
	Fifo150
	Dram

Write process:
	check if resource is available in case of specific commands
	check if memory is reachable in case of udma or mem
	check if width of word is compatible
		return success or fail

read process
	check if resource is available in case of specific commands
	check if memory is reachable in case of udma or mem
		return success or fail

0 -> Failure
1 -> Success
	*/
u32 send_buf[BUFF_SIZE];
u32 histo_buff[0x1FFFF];
u32 logging = true;
u32 XPAR_COMBLOCK_ID=0;

/*All initialization functions are set here */
static inline int init_funct(void){
	int status=0;
	status=pAmp_init();
	return status;
}

static inline int read_FIFO(UINTPTR baseaddr, u32 *send_buf, u32 length) {
	volatile u32 i = 0;
	while((cbRead(baseaddr, CB_IFIFO_STATUS) & 0x01) == 0) {
		if (length > 0 && i == length){	// read until empty or a specified length
			send_buf[0]=i;
			return i;
			break;
		}
		send_buf[i + 1] = cbRead(baseaddr, CB_IFIFO_VALUE);
		i++;
	}
    if (cbRead(baseaddr, CB_IFIFO_STATUS) & 0x04) {
    	send_buf[0]= 0; // if underflow there was an error
    	return 0;
    } else {
    	send_buf[0]= i;
    	return i;
    }
}

static inline void write_FIFO(UINTPTR baseaddr, u32 *recv_buf, u32 length, u32 *send_buf) {
	volatile u32 i = 0;
	while((cbRead(baseaddr, CB_OFIFO_STATUS) & 0x01) == 0 && i < length) {
		cbWrite(baseaddr, CB_OFIFO_VALUE, recv_buf[i + 2]);
		i++;
	}
    if (cbRead(baseaddr, CB_OFIFO_STATUS) & 0x04) {
    	send_buf[0]= 2; // if overflow there was an error
    } else {
    	send_buf[0]= 1;
    }
}

static inline void read_RAM(UINTPTR baseaddr, UINTPTR offset, u32 *send_buf, u32 length, u32 inc) {
	if (inc == 1) {
		if(logging)
			xil_printf("Reading: %d, %d, %d \n", baseaddr + offset, length, inc);
		cbReadBulk((int *)(send_buf + 1), baseaddr + offset, length);
	} else {
		volatile u32 i;
		for (i = 0; i < length; i ++) {
			if (logging)
				xil_printf("Reading: %d, %d \n", baseaddr + offset, i * inc * 4);
			send_buf[i + 1]= cbRead(baseaddr + offset, i * inc);
		}
	}
	if(length * inc * 4 > XComblock_ConfigTable[XPAR_COMBLOCK_ID].DRAM_IO_DEPTH - offset)
		send_buf[0]= 2; // if overflow there was an error
	else
		send_buf[0]= length;
}

static inline void write_RAM(UINTPTR baseaddr, UINTPTR offset, u32 *recv_buf, u32 length, u32 inc, u32 *send_buf) {
	if (inc == 1) {
		if(logging)
			xil_printf("Writing: %d, %d \n", baseaddr + offset, (int *)(recv_buf + 5));
		cbWriteBulk(baseaddr + offset, (int *) (recv_buf + 5), length);
	}else {
		volatile u32 i;
		for (i = 0; i < length; i ++) {
			if(logging)
				xil_printf("Writing: %d, %d, %d \n", baseaddr + offset, i * 4 * inc, recv_buf[i + 5]);
			cbWrite(baseaddr + offset, i * inc, recv_buf[i + 5]);
		}
	}
	if(logging)
		xil_printf("Partial success \n");
	if(length * inc * 4 > XComblock_ConfigTable[XPAR_COMBLOCK_ID].DRAM_IO_DEPTH - offset)
		send_buf[0]= 2; // if overflow there was an error
	else
		send_buf[0]= 1;
}

static inline void read_MEM(UINTPTR baseaddr, u32 *send_buf, u32 length, u32 inc) {
	if (inc == 1)
		memmove((UINTPTR *)baseaddr, send_buf + 1, length);
	else {
		volatile u32 i;
		for (i = 0; i < length; i ++) {
			send_buf[i + 1]= cbRead(baseaddr, i * inc);
		}
	}
	send_buf[0] = 1; // success always assumed, unprotected operations are fully responsibility of the user
}

static inline void write_MEM(UINTPTR baseaddr, u32 *recv_buf, u32 length, u32 inc, u32 *send_buf) {
	if (inc == 1)
		memmove((UINTPTR *)baseaddr, recv_buf, length);
	else {
		volatile u32 i;
		for (i = 0; i < length; i ++) {
			cbWrite(baseaddr, i * inc, recv_buf[i + 5]);
		}
	}
	send_buf[0] = 1; // success always assumed, unprotected operations are fully responsibility of the user
}

static inline int process_command(u32 *recv_buf, int sd) {
	send_buf[0]= 0; // always error unless a successful operation
	u32 pack_type = recv_buf[0];
	if(logging)
		xil_printf("\n------- Packet type:\t %d -------\n",pack_type);
	u32 o = 0;
	switch(pack_type){
		case READ_REG:
			if(logging)
				xil_printf("Register:\t %u \n", recv_buf[1]);
			if (XComblock_ConfigTable[XPAR_COMBLOCK_ID].REGS_IN_ENA) {
				send_buf[1] = cbRead(XComblock_ConfigTable[XPAR_COMBLOCK_ID].AXIL_BASEADDR, recv_buf[1]);
				send_buf[0] = 1;
				if(logging)
					xil_printf("READ VALUE:\t %u \n", send_buf[1]);
				return write(sd, send_buf, 2 * 4);
			} else
				return write(sd, send_buf, 4);
			break;
		case READ_RAM:
			if(logging)
				xil_printf("Address:\t %u N:\t %u Inc:\t %u\n",
						recv_buf[1], recv_buf[2], recv_buf[3]);
			if (XComblock_ConfigTable[XPAR_COMBLOCK_ID].DRAM_IO_ENA) {
				read_RAM(XComblock_ConfigTable[XPAR_COMBLOCK_ID].AXIF_BASEADDR, (UINTPTR) recv_buf[1] * 4,
					send_buf, recv_buf[2], recv_buf[3]);
				if(logging) {
					for(o = 0; o < recv_buf[2] + 1; o++)
						xil_printf("data: %u \n", send_buf[o]);
				}
				return write(sd, send_buf, (recv_buf[2] + 1) * 4);
			} else
				return write(sd, send_buf, 4);
			break;
		case READ_MEM:
			if(logging) {
				xil_printf("Warning, unprotected operation!");
				xil_printf("Address:\t %u N:\t %u Inc:\t %u\n",
						recv_buf[1], recv_buf[2], recv_buf[3]);
			}
			if(!(recv_buf[1] % 4))
				read_MEM((UINTPTR) recv_buf[1], send_buf, recv_buf[2], recv_buf[3]);
			else {
				if(logging)
					printf("Address must be a multiple of 4 due to the data width (4 bytes).\n");
			}
			if(logging) {
				for(o = 0; o < recv_buf[2] + 1; o++)
					xil_printf("data: %u \n", send_buf[o]);
			}
			return write(sd, send_buf, (recv_buf[2] + 1) * 4);
			break;
		case READ_FIFO:
			if(logging)
				xil_printf("FIFO N:\t %u \n", recv_buf[1]);
			if (XComblock_ConfigTable[XPAR_COMBLOCK_ID].FIFO_IN_ENA) {
				int tr=read_FIFO(XComblock_ConfigTable[XPAR_COMBLOCK_ID].AXIL_BASEADDR, send_buf, recv_buf[1]);
				if(logging) {
					xil_printf("To transmit: %d +1 words. \n\r",tr);
					for(o = 0; o < recv_buf[1] + 1; o++)
						xil_printf("data %d: %u \n\r",o, send_buf[o]);
				}
				return write(sd, send_buf, (tr + 1) * 4);
			} else
				return write(sd, send_buf, 4);
			break;
		case WRITE_REG:
			if(logging)
				xil_printf("Register:\t %u data:\t %u \n",recv_buf[1],recv_buf[2]);
			if (XComblock_ConfigTable[XPAR_COMBLOCK_ID].REGS_OUT_ENA) {
				cbWrite(XComblock_ConfigTable[XPAR_COMBLOCK_ID].AXIL_BASEADDR, recv_buf[1] + 16, recv_buf[2]);
				send_buf[0] = 1;
			}
			return write(sd, send_buf, 4);
			break;
		case WRITE_RAM:
			if(logging) {
				xil_printf("Addr:\t %u N:\t %u Inc:\t %u Rad:\t %u\n",
						recv_buf[1], recv_buf[2], recv_buf[3], recv_buf[4]);
				for(o = 0; o < recv_buf[2]; o++)
					xil_printf("data: %u \n", recv_buf[o + 5]);
			}
			if (XComblock_ConfigTable[XPAR_COMBLOCK_ID].DRAM_IO_ENA) {
				if(logging)
					xil_printf("Resources available\n");
				write_RAM(XComblock_ConfigTable[XPAR_COMBLOCK_ID].AXIF_BASEADDR, recv_buf[1] * 4,
					recv_buf, (UINTPTR) recv_buf[2], recv_buf[3], send_buf);
				if(logging)
					xil_printf("Operation completed with result %d \n", send_buf[0]);
			}
			return write(sd, send_buf, 4);
			break;
		case WRITE_MEM:
			if(logging) {
				xil_printf("Warning, non protected operation!\n");
				xil_printf("Addr:\t %u N:\t %u Inc:\t %u Rad:\t %u\n",
						recv_buf[1], recv_buf[2], recv_buf[3], recv_buf[4]);
				for(o = 0; o < recv_buf[2]; o++)
					xil_printf("data: %u \n", recv_buf[o + 5]);
			}
			if(!(recv_buf[1] % 4))
				write_MEM(recv_buf[1], recv_buf, recv_buf[2], recv_buf[3], send_buf);
			else {
				if(logging)
					printf("Address must be a multiple of 4 due to the data width (4 bytes).\n");
			}
			return write(sd, send_buf, 4);
			break;
        case WRITE_FIFO:
        	if(logging) {
        		xil_printf("FIFO N:\t %u \n", recv_buf[1]);
        		for(o = 0; o < recv_buf[1]; o++)
        			xil_printf("data: %u \n", recv_buf[o + 2]);
        	}
			if (XComblock_ConfigTable[XPAR_COMBLOCK_ID].FIFO_OUT_ENA) {
				write_FIFO(XComblock_ConfigTable[XPAR_COMBLOCK_ID].AXIL_BASEADDR, recv_buf, recv_buf[1], send_buf);
			}
			return write(sd, send_buf, 4);
            break;
		case UDMA:
			// write in special memory for UDMA module to process
			break;
		case LOG:
			logging ^= true;
			if(logging) {
				xil_printf("Logging enabled. \n");
			}else{
				xil_printf("Logging disabled. \n");}
		case CHANGE_CB:
			if(logging) {
				xil_printf("Change active comblock. \r\n");
			}
			if (XPAR_COMBLOCK_NUM_INSTANCES>1 && (XPAR_COMBLOCK_NUM_INSTANCES-recv_buf[1])>0) {
				if(logging)
					xil_printf("ACTIVE COMBLOCK:\t %u \n", recv_buf[1]);
				XPAR_COMBLOCK_ID = recv_buf[1];
				send_buf[0] = 1;
			} else{
				if(logging)
					xil_printf("COMBLOCK ID NOT VALID:\t %u \r\n", send_buf[1]);
				XPAR_COMBLOCK_ID =0;
				send_buf[0] = 0;
			}

			return write(sd, send_buf, 4);
			break;

		//ADC500 AND PAMP RELATED FUNCTIONS
		case ADC500_CFG_FUNC:
			if(logging)
				xil_printf("ADC500_CFG to:\t %u \n",recv_buf[1]);
			if (XComblock_ConfigTable[XPAR_COMBLOCK_ID].REGS_OUT_ENA) {
				send_buf[1] = ADC500_CFG(recv_buf[1]);
				send_buf[0] = 1;
			}
			if(logging)
				xil_printf("ADC500 status:\t %u \n", send_buf[1]);
			return write(sd, send_buf, 8);
			break;
		case DEC_CFG:
			if(logging)
				xil_printf("SET DEC to:\t %u \r\n",recv_buf[1]);
			if (XComblock_ConfigTable[XPAR_COMBLOCK_ID].REGS_OUT_ENA) {
				send_buf[1] = SET_DEC(recv_buf[1]);
				send_buf[0] = 1;
			}
			if(logging)
				xil_printf("SET_DEC status:\t %u \n", send_buf[1]);
			return write(sd, send_buf, 8);
			break;
		case PAMP_INIT:
			if(logging)
				xil_printf("Initialize pAs:\t\n");
			if (XComblock_ConfigTable[XPAR_COMBLOCK_ID].REGS_OUT_ENA) {
				send_buf[1] = pAmp_init();
				send_buf[0] = 1;
			}
			if(logging)
				xil_printf("Init_pAs status:\t %u \n", send_buf[1]);
			return write(sd, send_buf, 8);
			break;
		case VADJ_SET:
			if(logging)
				xil_printf("Set VADJ on CIAA to:\t %u \n", recv_buf[1]);
			if (XComblock_ConfigTable[XPAR_COMBLOCK_ID].REGS_OUT_ENA) {
				send_buf[1] = vadj_set(recv_buf[1]);
				send_buf[0] = 1;
			}
			if(logging)
				xil_printf("VAD set to:\t %u \n", send_buf[1]);
			return write(sd, send_buf, 8);
			break;
		case PAS_SET:
			if(logging)
				xil_printf("Set pAmp Static signals to:\t %u \n", recv_buf[1]);
			if (XComblock_ConfigTable[XPAR_COMBLOCK_ID].REGS_OUT_ENA) {
				send_buf[1] = pAs_set(recv_buf[1]);
				send_buf[0] = 1;
			}
			if(logging)
				xil_printf("pAmp Static set to:\t %u \n", send_buf[1]);
			return write(sd, send_buf, 8);
			break;
		case PAMP_POT:
			if(logging)
				xil_printf("Set pAmp potentiometer \t %d with the value %u \n", recv_buf[1], recv_buf[2]);
			if (XComblock_ConfigTable[XPAR_COMBLOCK_ID].REGS_OUT_ENA) {
				send_buf[1] = pAmp_pot(recv_buf[1],recv_buf[2]);
				send_buf[0] = 1;
			}
			if(logging)
				xil_printf("pAmp Static set to:\t %u \n", send_buf[1]);
			return write(sd, send_buf, 8);
			break;
		case PAMP_GEN_HISTO:
			if (logging)
				xil_printf("Generate histogram \t %d counts and %d continuous and %d \n\r", recv_buf[1], recv_buf[2], recv_buf[3]);
			if (XComblock_ConfigTable[XPAR_COMBLOCK_ID].REGS_OUT_ENA) {
				send_buf[0]=pAmp_Gen_histo(&HGM, recv_buf[1], recv_buf[2], recv_buf[3]);
				return write(sd, send_buf, 4);
			} else{
				send_buf[0]=0;
				return write(sd, send_buf, 4);
			}
			break;
		case PAMP_GET_HISTO:
			if (logging)
				xil_printf("Get histogram data \n\r");
			if (XComblock_ConfigTable[XPAR_COMBLOCK_ID].REGS_OUT_ENA) {
				pAmp_Get_histo(&HGM, histo_buff);
				return write(sd, histo_buff, (HGM.Memory_bins + 3) * 4);
			} else{
				send_buf[0]=0;
				return write(sd, send_buf, 4);
			}
			break;
/*HISTOGRAM RELATED OPTIONS*******
#define HISTO_CLEAN			200
#define HISTO_SET			201
#define HISTO_ENABLE		202
#define HISTO_READ			203
#define HISTO_RDCOUNT		204
**********************************/
		case HISTO_CLEAN:
			if(logging)
				xil_printf("HISTO_CLEAN:\t %u \n", recv_buf[1]);
			if (XComblock_ConfigTable[XPAR_COMBLOCK_ID].REGS_IN_ENA) {
				send_buf[0] = clean_TDPR(&HGM);
				if(logging)
					xil_printf("Status:\t %u \r\n", send_buf[1]);
				return write(sd, send_buf, 4);
			} else
				return write(sd, send_buf, 4);
			break;
		case HISTO_SET:
			if(logging)
				xil_printf("HISTO_SET_N:\t %u \r\n",recv_buf[1]);
			if (XComblock_ConfigTable[XPAR_COMBLOCK_ID].REGS_OUT_ENA) {
				send_buf[0] = Histo_SetCounts(&HGM, recv_buf[1]);
			}
			return write(sd, send_buf, 4);
			break;
		case HISTO_ENABLE:
			if(logging)
				xil_printf("HISTO_ENABLE:\t %u \r\n",recv_buf[1]);
			if (XComblock_ConfigTable[XPAR_COMBLOCK_ID].REGS_OUT_ENA) {
				send_buf[0] =1;
				send_buf[1] = Histo_count_enable(&HGM, recv_buf[1]);
				if(logging)
					xil_printf("CTRL_REG status %d",send_buf[1]);
			}
			return write(sd, send_buf, 2*4);
			break;
		case HISTO_RDCOUNT:
			if(logging)
				xil_printf("HISTO_Read Count:\r\n");
			if (XComblock_ConfigTable[XPAR_COMBLOCK_ID].REGS_OUT_ENA) {
				send_buf[0] =1;
				send_buf[1] = Histo_Get_Curr_Counts(&HGM);
				if(logging)
					xil_printf("Histogram Current count status %d",send_buf[1]);
			}
			return write(sd, send_buf, 2*4);
			break;
		case HISTO_ISDONE:
			if(logging)
				xil_printf("HISTO Read Done Signal:\r\n");
			if (XComblock_ConfigTable[XPAR_COMBLOCK_ID].REGS_OUT_ENA) {
				send_buf[0] =1;
				send_buf[1] = Histo_is_done(&HGM);
				if(logging)
					xil_printf("Histogram Current count status %d",send_buf[1]);
			}
			return write(sd, send_buf, 2*4);
			break;
		case HISTO_READ:
			if(logging)
				xil_printf("HISTOGRAM READ \n\r");
			if (XComblock_ConfigTable[XPAR_COMBLOCK_ID].FIFO_IN_ENA) {
				int tr=Histo_rfm(&HGM, &send_buf[1]);
				if(logging) {
					xil_printf("To transmit: %d +1 words. \n\r",tr);
					send_buf[0]=tr;
//					for(o = 0; o < tr + 1; o++)
//						xil_printf("BIN %d: %u \n\r",o, send_buf[o]);
				}
				return write(sd, send_buf, (tr + 1) * 4);
			} else{
				send_buf[0]=0;
				return write(sd, send_buf, 4);
			}

			break;
		default:
			return 0;
			break;

	}
	return 0;
}

#endif //UDMA_H
